﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;

    public Vector3 offSet = new Vector3(0, 5, -10);

    void Update()
    {
        Vector3 targetPosition = target.transform.position + offSet;

        // Smoothly move the camera towards that target position
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        transform.rotation = gameObject.transform.rotation;
    }
}

