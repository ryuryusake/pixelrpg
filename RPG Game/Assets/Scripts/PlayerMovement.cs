using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Values")]
    public float realtimeSpeed;
    public float walkSpeed;
    public float runSpeed;
    public float jumpVelocity;

    public Vector2 direction;

    [Header("Animator")]
    public GameObject playerRotation;
    public Animator animator;

    [Header("Physics")]
    //General
    public GameObject playerController;
    public Transform playerTransform;
    public Rigidbody2D RB;

    //Jump
    public bool isGrounded;
    public LayerMask groundLayer;
    public Transform checkGround;
    public float checkRadius;

    public float hangTime = .2f;
    private float hangCounter;

    [Header("Special Buttons")]
    public GameObject worldMap;

    [Header("VFX")]
    public List<ParticleSystem> particleList;

    // Start is called before the first frame update
    private void Start()
    {
        playerTransform = playerRotation.GetComponent<Transform>();
    }

    // Update is called once per frame
    private void Update()
    {
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        isGrounded = Physics2D.OverlapCircle(checkGround.position, checkRadius, groundLayer);

        //Run
        if (Input.GetButton("LeftShift"))
        {
            realtimeSpeed = runSpeed;
            animator.SetBool("isRunning", true);
            if (!particleList[0].isPlaying)
            {
                particleList[0].Play();
            }
        }
        else
        {
            realtimeSpeed = walkSpeed;
            animator.SetBool("isRunning", false);
        }

        //Jump Button
        if (Input.GetButtonDown("Jump") && isGrounded == true && hangCounter > 0f)
        {
            Jump();
        }

        //Ground Check
        if (isGrounded == true)
        {
            animator.SetBool("isGrounded", true);
            hangCounter = hangTime;
        }
        else
        {
            animator.SetBool("isGrounded", false);
            hangCounter -= Time.deltaTime;
        }

        //Use Card
        
        //VFX
        if (Mathf.Abs(RB.velocity.x) < 0.1)
        {
            if (particleList[0].isPlaying)
            {
                particleList[0].Stop();
            }
        }
    }

    private void FixedUpdate()
    {
        //Move Character
        if (Input.GetButton("Horizontal"))
        {
            MoveCharacter(direction.x);
        }
    }

    void MoveCharacter(float horizontal)
    {
        RB.AddForce(Vector2.right * horizontal * realtimeSpeed);
        animator.SetFloat("horizontalSpeed", Mathf.Abs(RB.velocity.x));
        FlipSprite();
        if (Mathf.Abs(RB.velocity.x) > walkSpeed)
        {
            RB.velocity = new Vector2(Mathf.Sign(RB.velocity.x) * walkSpeed, RB.velocity.y);
        }
    }

    void Jump()
    {
        RB.velocity = new Vector2(RB.velocity.x, jumpVelocity);
        RB.AddForce(Vector2.up * jumpVelocity, ForceMode2D.Impulse);
        animator.SetTrigger("jumpTrigger");
    }

    void FlipSprite()
    {
        if (direction.x > 0)
        {
            playerRotation.transform.eulerAngles = new Vector3(0, 180, 0);
        }
        else
        {
            playerRotation.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }
}