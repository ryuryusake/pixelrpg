using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator animator;
    public GameObject playerController;
    public GameObject worldMap;
   
    // Start is called before the first frame update
    private void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        animator.SetFloat("horizontalSpeed", Mathf.Abs(Input.GetAxis("Horizontal")));
        animator.SetFloat("verticalSpeed", Input.GetAxis("Vertical"));

        //Open/Close World-Map
        if (Input.GetButton("ToggleMap"))
        {
            worldMap.SetActive(true);
            Debug.Log("map = on");
        }
        else
        {
            worldMap.SetActive(false);
        }
    }
}
