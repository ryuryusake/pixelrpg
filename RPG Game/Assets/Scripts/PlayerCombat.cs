using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour
{
    [Header("Components")]
    public Animator animator;

    public Transform pointOfAttack;
    public float attackRange;
    public int attackDamage;
    public float attackRate;
    private float nextAttackTime;
    public Vector2 knockbackVelocity;

    public LayerMask enemyLayers;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time >= nextAttackTime) //Check if you are able to attack, and attack.
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Attack();
                nextAttackTime = Time.time + 1f / attackRate;
            }
        }
    }

    void Attack()
    {
        //Animate
        animator.SetTrigger("attackTrigger");

        //Deal damage
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(pointOfAttack.position, attackRange, enemyLayers);
        foreach(Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<EnemyController>().TakeDamage(attackDamage);
            enemy.attachedRigidbody.velocity = knockbackVelocity;
            Debug.Log("Damage dealt on:" + enemy.name);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (pointOfAttack == null)
            return;
        Gizmos.DrawWireSphere(pointOfAttack.position, attackRange);
    }
}
