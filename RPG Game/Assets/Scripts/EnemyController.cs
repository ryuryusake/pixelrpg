using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int maxHealth = 100;
    int currentHealth;
    public GameObject rig;

    [Header("VFX")]
    public List<ParticleSystem> particleList;
    public Transform particleTransform;
    public GameObject deathParticle;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        //SetTrigger("attackTrigger")();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        rig.GetComponent<Animator>().SetTrigger("getHitTrigger");
        particleList[0].Play();
        //Play Damage Particle
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        //Play Death Particle
        Instantiate(deathParticle, particleTransform.transform.position, Quaternion.identity);
        Debug.Log("Enemy defeated!");
        Destroy(gameObject);
    }
}
